package com.einfoplanet.demo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.einfoplanet.demo.R
import com.einfoplanet.demo.databinding.SingleItemRestaurantBinding
import com.einfoplanet.demo.listeners.RestaurantClickedEventListener
import com.einfoplanet.demo.model.Restaurants

/**
 * Adapter to load the Grid of restaurants data in RestaurantFragment.
 */
class RestaurantGridAdapter constructor(
    private val restaurantClickEventListener: RestaurantClickedEventListener,
    internal var context: Context,
    internal var nearbyRestaurants: List<Restaurants.NearbyRestaurant>
) : RecyclerView.Adapter<RestaurantGridAdapter.CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val binding = DataBindingUtil.inflate<SingleItemRestaurantBinding>(
            LayoutInflater.from(context),
            R.layout.single_item_restaurant, parent, false
        )
        return CustomViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return nearbyRestaurants.size
    }

    fun setList(nearbyRestaurants: List<Restaurants.NearbyRestaurant>) {
        this.nearbyRestaurants = nearbyRestaurants
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bind(restaurantData = nearbyRestaurants[position].restaurant)
    }

    inner class CustomViewHolder(itemView: SingleItemRestaurantBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        private var binding: SingleItemRestaurantBinding? = null

        fun bind(restaurantData: Restaurants.NearbyRestaurant.Restaurant) {
            if (binding == null)
                binding = DataBindingUtil.bind(itemView)

            binding!!.restaurantClickEventListener = restaurantClickEventListener
            binding!!.viewModel = restaurantData
            binding!!.txtCost.text =
                String.format("Cost for two ₹%s approx", restaurantData.averageCostForTwo)
        }
    }
}
