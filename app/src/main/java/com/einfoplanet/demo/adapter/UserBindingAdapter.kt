package com.einfoplanet.demo.adapter

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.einfoplanet.demo.R
import com.einfoplanet.demo.model.Restaurant
import com.einfoplanet.demo.util.UiUtils

@BindingAdapter("goneUnless")
fun goneUnless(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}

//When both the data from db & api are empty visible the no data text view
@BindingAdapter(value = ["dbUserListSize", "apiUserListSize"], requireAll = true)
fun goneIfEmptyList(
    view: View,
    dbUserListSize: Int,
    apiUserListSize: Int
) {
    view.visibility = if (dbUserListSize > 0 || apiUserListSize > 0) View.GONE else View.VISIBLE
}

@BindingAdapter("pageMargin")
fun pageMargin(viewPager: ViewPager, pageMargin: Float) {
    viewPager.pageMargin = pageMargin.toInt()
}

@BindingAdapter("imageUrl")
fun bindImage(imageView: ImageView, imagePath: String?) {

    imagePath?.let {
        Glide.with(imageView.context)
            .load(imagePath)
            .placeholder(R.color.grey_600)
            .into(imageView)
    }
}

@BindingAdapter("visibleGone")
fun showHide(view: View, show: Boolean) {
    if (show) view.visibility = View.VISIBLE else view.visibility = View.GONE
}

@BindingAdapter("photosItems")
fun setPhotosItems(recyclerView: RecyclerView, items: List<Restaurant.Photo>?) {
    val adapter = recyclerView.adapter as PhotosGridAdapter?
    items?.let {
        adapter?.setList(items)
    }
}

@BindingAdapter("ratingColor")
fun setTextViewTint(textView: TextView, colorValue: String?) {

    textView.background = (textView.background as? GradientDrawable ?: GradientDrawable()).apply {
        setTint(Color.parseColor("#$colorValue"))
    }
}