package com.einfoplanet.demo.ui.rewards

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import com.einfoplanet.demo.R
import com.einfoplanet.demo.databinding.FragmentRewardsBinding
import com.einfoplanet.demo.listeners.MainNavigationFragment
import com.einfoplanet.demo.util.inTransaction

class RewardsFragment : Fragment() {

    companion object {
        private const val FRAGMENT_ID = R.id.fragment_container
    }

    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var currentFragment: MainNavigationFragment


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentRewardsBinding.inflate(inflater, container, false)
        coordinatorLayout = binding.coordinatorLayout
        return binding.root
    }


    private fun <F> replaceFragment(fragment: F) where F : Fragment, F : MainNavigationFragment {
        activity!!.supportFragmentManager.inTransaction {
            currentFragment = fragment
            replace(FRAGMENT_ID, fragment)
        }
    }
}