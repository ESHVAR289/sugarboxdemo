package com.einfoplanet.demo.ui.launcher

import android.Manifest
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import com.einfoplanet.demo.R
import com.einfoplanet.demo.ui.RestaurantActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

class SplashActivity : AppCompatActivity() {

    private val TAG = SplashActivity::class.java.simpleName
    private val locationPermissionRequestCode = 106

    companion object {
        const val GPS_SOURCE_ENABLE_REQUEST: Int = 192
        const val PERMISSION_ENABLE_REQUEST: Int = 193
        const val IS_FIRST_TIME_DIALOG_SHOWN: String = "IS_FIRST_TIME_DIALOG_SHOWN"
        const val CURRENT_LATITUDE: String = "CURRENT_LATITUDE"
        const val CURRENT_LONGITUDE: String = "CURRENT_LONGITUDE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val sharedPref = getPreferences(Context.MODE_PRIVATE) ?: return
        val isInfoDialogShown = sharedPref.getBoolean(IS_FIRST_TIME_DIALOG_SHOWN, false)

        if (!isInfoDialogShown) showFirstTimeDialog() else checkForLocationPermission()
    }

    private fun checkForLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (PermissionChecker.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PermissionChecker.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showPermissionAlert()
                } else {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ),
                        locationPermissionRequestCode
                    )
                }
            } else if (PermissionChecker.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PermissionChecker.PERMISSION_GRANTED
            ) {
                showPermissionAlert()
            } else {
                checkGpsProviderEnabled()
            }
        } else {
            checkGpsProviderEnabled()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == locationPermissionRequestCode) {
            if (grantResults[0] == PermissionChecker.PERMISSION_GRANTED) {
                checkGpsProviderEnabled()
            } else if (grantResults[0] == PermissionChecker.PERMISSION_DENIED) {
                showPermissionAlert()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GPS_SOURCE_ENABLE_REQUEST) checkGpsProviderEnabled()
        if (requestCode == PERMISSION_ENABLE_REQUEST) checkForLocationPermission()
    }

    private fun fetchLatLong() {
        this.let { activity ->
            val fusedLocationProviderClient: FusedLocationProviderClient =
                LocationServices.getFusedLocationProviderClient(activity)
            fusedLocationProviderClient.lastLocation.addOnSuccessListener { location ->
                Log.e(TAG, "LATITUDE ${location?.latitude} : LONGITUDE ${location?.longitude}")
                Handler().postDelayed({
                    startActivity(Intent(this, RestaurantActivity::class.java).also {
                        it.putExtra(CURRENT_LATITUDE, location.latitude)
                        it.putExtra(CURRENT_LONGITUDE, location.longitude)
                    })
                    finish()
                }, 1000)
            }
        }
    }

    private fun checkGpsProviderEnabled() {
        val locationManager: LocationManager =
            this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var gps_enabled = false

        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        } catch (e: Exception) {
        }

        Log.e(TAG, "GPS : $gps_enabled")
        if (!gps_enabled) showGpsPermissionAlert(GPS_SOURCE_ENABLE_REQUEST) else fetchLatLong()
    }

    private fun showPermissionAlert() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.err_location_permission_denied))
        builder.setMessage(getString(R.string.msg_zomato_permission_use))
        builder.setNegativeButton(getString(R.string.label_i_am_sure)) { _, _ ->
            finish()
        }

        builder.setPositiveButton(getString(R.string.label_retry)) { _, _ ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                val uri = Uri.fromParts("package", packageName, null)
                intent.data = uri
                startActivityForResult(intent, PERMISSION_ENABLE_REQUEST)
            }
        }
        val alertDialog = builder.create().apply {
            setCanceledOnTouchOutside(false)
            show()
        }

        alertDialog.setOnCancelListener {
            onBackPressed()
        }
    }

    private fun showGpsPermissionAlert(permissionCode: Int) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.err_gps_permission_denied))

        builder.setMessage(getString(R.string.msg_zomato_permission_use))
        builder.setNegativeButton(getString(R.string.label_i_am_sure)) { _, _ ->
            finish()
        }

        builder.setPositiveButton(getString(R.string.label_enable_gps)) { _, _ ->
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivityForResult(intent, permissionCode)
        }
        val alertDialog = builder.create().apply {
            setCanceledOnTouchOutside(false)
            show()
        }

        alertDialog.setOnCancelListener {
            onBackPressed()
        }
    }

    fun showFirstTimeDialog() {
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.info_layout, null)
        dialogBuilder.setView(dialogView)

        val alertDialog = dialogBuilder.create().apply {
            setCanceledOnTouchOutside(false)
            show()
        }

        alertDialog.setOnCancelListener {
            onBackPressed()
        }

    }

    fun enableLocation(view: View) {
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        with(sharedPref.edit()) {
            putBoolean(IS_FIRST_TIME_DIALOG_SHOWN, true)
            commit()
        }

        checkForLocationPermission()
    }
}
