package com.einfoplanet.demo.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.adroitandroid.chipcloud.ChipCloud
import com.adroitandroid.chipcloud.ChipListener
import com.einfoplanet.demo.R
import com.einfoplanet.demo.adapter.RestaurantGridAdapter
import com.einfoplanet.demo.databinding.FragmentRestaurantsBinding
import com.einfoplanet.demo.listeners.MainNavigationFragment
import com.einfoplanet.demo.ui.launcher.SplashActivity.Companion.CURRENT_LATITUDE
import com.einfoplanet.demo.ui.launcher.SplashActivity.Companion.CURRENT_LONGITUDE
import com.einfoplanet.demo.ui.restaurantdetail.RestaurantDetailsActivity
import com.einfoplanet.demo.util.ItemOffsetDecoration
import com.einfoplanet.demo.util.NetworkUtils
import com.einfoplanet.demo.util.viewModelProvider
import kotlinx.android.synthetic.main.fragment_restaurants.*
import org.jetbrains.anko.runOnUiThread

/**
 * This is the fragment which is used to show the list of restaurants.
 * On restaurant selection we are redirecting user to restaurant detail page.
 */
class RestaurantsFragment : Fragment(), MainNavigationFragment {
    companion object {
        private val TAG = RestaurantsFragment::class.java.simpleName
        private const val ARG_SELECTED_FRAGMENT = "arg.SELECTED_FRAGMENT"
        const val ARG_SELECTED_RESTAURANT_ID = "arg.SELECTED_RESTAURANT_ID"
    }

    private lateinit var viewModel: RestaurantsViewModel
    private lateinit var binding: FragmentRestaurantsBinding
    private lateinit var restaurantAdapter: RestaurantGridAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = viewModelProvider()
        binding = FragmentRestaurantsBinding.inflate(inflater, container, false).apply {
            setLifecycleOwner(this@RestaurantsFragment)
            viewModel = this@RestaurantsFragment.viewModel
        }
        restaurantAdapter = RestaurantGridAdapter(viewModel, requireContext(), emptyList())
        val itemDecoration = ItemOffsetDecoration(activity!!, R.dimen.item_offset)
        binding.recyclerview.apply {
            adapter = this@RestaurantsFragment.restaurantAdapter
            layoutManager = LinearLayoutManager(requireContext())
            addItemDecoration(itemDecoration)
        }

        arguments?.let {
            val bundle: Bundle = arguments as Bundle
            bundle.let {
                viewModel.currentLatitude = it.getDouble(CURRENT_LATITUDE)
                viewModel.currentLongitude = it.getDouble(CURRENT_LONGITUDE)
            }
        }

        viewModel.currentLatitude = activity?.intent?.getDoubleExtra(CURRENT_LATITUDE, 0.0)!!
        viewModel.currentLongitude = activity?.intent?.getDoubleExtra(CURRENT_LONGITUDE, 0.0)!!

        initViewModel()
        return binding.root
    }

    private fun initViewModel() {

        viewModel.restaurantLiveData.observe(this, Observer {
            restaurantAdapter.setList(it)
        })

        viewModel.strArea.observe(this, Observer {
            txt_address.text = it
        })

        viewModel.popularityLiveData.observe(this, Observer {
            setUpChipCloud(it.topCuisines.toTypedArray())
        })

        //Show an error message
        viewModel.errorMessage.observe(this, Observer {
            it?.let { errorMessage ->
                context!!.runOnUiThread {
                    when (it) {
                        "Something bad happen. Please try again !!" -> binding.txtError.visibility =
                            View.VISIBLE
                        "No Favourites found !!" -> binding.txtError.visibility =
                            View.VISIBLE
                        "No cuisine found !!" -> binding.txtError.visibility =
                            View.VISIBLE
                        else -> binding.txtError.visibility = View.GONE
                    }
                }
            }
        })

        viewModel.clickedRestaurantData.observe(this, Observer {
            val intent = Intent(activity, RestaurantDetailsActivity::class.java)
            intent.putExtra(ARG_SELECTED_RESTAURANT_ID, it.id)
            startActivity(intent)
        })

        checkNetworkAndLoadData()
    }

    private fun setUpChipCloud(toTypedArray: Array<String>) {
        ChipCloud.Configure()
            .chipCloud(binding.chipCloud)
            .labels(toTypedArray)
            .chipListener(object : ChipListener {
                override fun chipSelected(index: Int) {
                    //Filtering the restaurant data based on the chip selected.
                    //Checking if cuisine sting contains the selected chip
                    viewModel.updatedList = viewModel.restaurantLiveData.value!!.filter {
                        it.restaurant.cuisines.contains(toTypedArray[index])
                    }

                    restaurantAdapter.setList(viewModel.updatedList)

                    if (viewModel.updatedList.isNotEmpty())
                        viewModel.errorMessage.value = "Data available"
                    else {
                        viewModel.errorMessage.value = "No cuisine found !!"
                    }
                }

                override fun chipDeselected(index: Int) {
                    restaurantAdapter.setList(viewModel.restaurantLiveData.value!!)
                    viewModel.errorMessage.value = "Data available"
                }
            })
            .build()

    }

    private fun checkNetworkAndLoadData() {
        if (NetworkUtils(context = context!!).hasNetworkConnection()) {
            loadDataFromNetwork()
        } else {
            Toast.makeText(
                activity,
                getString(R.string.error_no_internet),
                Toast.LENGTH_SHORT
            ).show()
        }
    }


    private fun loadDataFromNetwork() {
        if (NetworkUtils(context = context!!).hasNetworkConnection()) {
            viewModel.getNearbyRestaurants(
                viewModel.currentLatitude,
                viewModel.currentLongitude
            )
        } else Toast.makeText(
            activity,
            getString(R.string.error_no_internet),
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun onBackPressed(): Boolean {
        return true
    }

}