package com.einfoplanet.demo.ui.home

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.einfoplanet.demo.listeners.RestaurantClickedEventListener
import com.einfoplanet.demo.model.Restaurants
import com.einfoplanet.demo.rest.ApiResponse
import com.einfoplanet.demo.rest.ApiService
import com.einfoplanet.demo.rest.Status
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class RestaurantsViewModel : ViewModel(),
    RestaurantClickedEventListener {
    companion object {
        private val compositeDisposable = CompositeDisposable()
        private val networkService = ApiService.getApiService()
    }

    private val _restaurantLiveData: MutableLiveData<List<Restaurants.NearbyRestaurant>> by lazy { MutableLiveData<List<Restaurants.NearbyRestaurant>>() }
    val restaurantLiveData: MutableLiveData<List<Restaurants.NearbyRestaurant>>
        get() = _restaurantLiveData

    private val _popularityLiveData: MutableLiveData<Restaurants.Popularity> by lazy { MutableLiveData<Restaurants.Popularity>() }
    val popularityLiveData: MutableLiveData<Restaurants.Popularity>
        get() = _popularityLiveData

    private val _isLoading: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }
    val isLoading: MutableLiveData<Boolean>
        get() = _isLoading

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: MutableLiveData<String>
        get() = _errorMessage

    private val _strArea = MutableLiveData<String>()
    val strArea: MutableLiveData<String>
        get() = _strArea

    private val _clickedRestaurantData: MutableLiveData<Restaurants.NearbyRestaurant.Restaurant> by lazy { MutableLiveData<Restaurants.NearbyRestaurant.Restaurant>() }
    val clickedRestaurantData: MutableLiveData<Restaurants.NearbyRestaurant.Restaurant>
        get() = _clickedRestaurantData

    var currentLatitude: Double = 0.0
    var currentLongitude: Double = 0.0
    var updatedList: List<Restaurants.NearbyRestaurant> = emptyList()

    fun getNearbyRestaurants(currentLatitude: Double, currentLongitude: Double) {
        compositeDisposable.add(
            networkService.getNearbyRestaurants(currentLatitude, currentLongitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    consumeResponse(ApiResponse.loading())
                }.subscribe({ result ->
                    consumeResponse(ApiResponse.success(result))
                }, { throwable ->
                    consumeResponse(ApiResponse.success(throwable))
                })
        )
    }

    fun searchRestaurants(
        currentLatitude: Double,
        currentLongitude: Double,
        selectedCategory: String
    ) {
        compositeDisposable.add(
            networkService.searchRestaurant(
                category = selectedCategory,
                latitude = currentLatitude,
                longitude = currentLongitude
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    consumeResponse(ApiResponse.loading())
                }.subscribe({ result ->
                    consumeResponse(ApiResponse.success(result))
                }, { throwable ->
                    consumeResponse(ApiResponse.success(throwable))
                })
        )
    }

    private fun consumeResponse(apiResponse: ApiResponse) {
        when (apiResponse.status) {
            Status.LOADING -> {
                _isLoading.value = true
            }

            Status.ERROR -> {
                _isLoading.value = false
                _restaurantLiveData.value = null
                _errorMessage.value = "Something bad happen. Please try again !!"
            }

            Status.SUCCESS -> {
                _isLoading.value = false
                if (apiResponse.data is Restaurants && apiResponse.data.restaurants != null)
                    _restaurantLiveData.value = apiResponse.data.restaurants
                else if (apiResponse.data is Restaurants && apiResponse.data.nearByRestaurant != null) {
                    _restaurantLiveData.value = apiResponse.data.nearByRestaurant
                    _popularityLiveData.value = apiResponse.data.popularity
                    _strArea.value = apiResponse.data.location.title
                }

            }
        }
    }

    override fun onRestaurantSelected(restaurant: Restaurants.NearbyRestaurant.Restaurant) {
        _clickedRestaurantData.value = restaurant
    }
}