package com.einfoplanet.demo.ui.restaurantdetail

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.einfoplanet.demo.model.Restaurant
import com.einfoplanet.demo.rest.ApiResponse
import com.einfoplanet.demo.rest.ApiService
import com.einfoplanet.demo.rest.Status
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class RestaurantDetailViewModel : ViewModel() {
    companion object {
        private val compositeDisposable = CompositeDisposable()
        private val networkService = ApiService.getApiService()
    }


    private val _restaurantLiveData: MutableLiveData<Restaurant> by lazy { MutableLiveData<Restaurant>() }
    val restaurantLiveData: MutableLiveData<Restaurant>
        get() = _restaurantLiveData

    private val _isLoading: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }
    val isLoading: MutableLiveData<Boolean>
        get() = _isLoading


    private val _errorMessage = MediatorLiveData<String>()
    val errorMessage: MutableLiveData<String>
        get() = _errorMessage


    fun getRestaurantDetails(restaurantId: String) {
        compositeDisposable.add(
            networkService.getRestaurantDetail(restaurantId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    consumeResponse(ApiResponse.loading())
                }.subscribe({ result ->
                    consumeResponse(ApiResponse.success(result))
                }, { throwable ->
                    consumeResponse(ApiResponse.success(throwable))
                })
        )
    }


    private fun consumeResponse(apiResponse: ApiResponse) {
        when (apiResponse.status) {
            Status.LOADING -> {
                _isLoading.value = true
            }

            Status.ERROR -> {
                _isLoading.value = false
                _restaurantLiveData.value = null
                _errorMessage.value = "Something bad happen. Please try again !!"
            }

            Status.SUCCESS -> {
                _isLoading.value = false
                if (apiResponse.data is Restaurant)
                    _restaurantLiveData.value = apiResponse.data
            }
        }
    }
}
