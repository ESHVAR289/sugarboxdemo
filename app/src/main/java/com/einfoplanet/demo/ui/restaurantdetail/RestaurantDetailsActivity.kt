package com.einfoplanet.demo.ui.restaurantdetail

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.adroitandroid.chipcloud.ChipCloud.Configure
import com.einfoplanet.demo.R
import com.einfoplanet.demo.adapter.PhotosGridAdapter
import com.einfoplanet.demo.databinding.ActivityRestaurantDetailBinding
import com.einfoplanet.demo.ui.home.RestaurantsFragment.Companion.ARG_SELECTED_RESTAURANT_ID
import com.einfoplanet.demo.util.viewModelProvider
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener


/**
 * This is the Detail page activity which is used to show the selected restaurant detail.
 */
class RestaurantDetailsActivity : AppCompatActivity() {
    private lateinit var mBinding: ActivityRestaurantDetailBinding
    private lateinit var mViewModel: RestaurantDetailViewModel
    private var selectedRestaurantId: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        selectedRestaurantId = intent.getStringExtra(ARG_SELECTED_RESTAURANT_ID)

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_restaurant_detail)
        mBinding.lifecycleOwner = this
        mViewModel = viewModelProvider()
        initViewModel()
        setupToolbar()
        setupPhotosAdapter()
    }

    private fun setUpChipCloud(toTypedArray: Array<String>) {
        Configure()
            .chipCloud(mBinding.chipCloud)
            .labels(toTypedArray)
            .build()

    }

    private fun initViewModel() {
        mViewModel.restaurantLiveData.observe(this, Observer {
            mBinding.restaurantDetail = it

            setUpChipCloud(it.highlights.filter { str ->
                str.length < 12
            }.toTypedArray())
        })


        //Show an error message
        mViewModel.errorMessage.observe(this, Observer {
            it?.let { errorMessage ->
                Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
            }
        })

        mViewModel.getRestaurantDetails(selectedRestaurantId!!)
    }

    private fun setupPhotosAdapter() {
        val listCast = mBinding.recyclerview
        listCast.layoutManager = GridLayoutManager(this,3)
        listCast.itemAnimator = DefaultItemAnimator()
        listCast.adapter = PhotosGridAdapter(this, emptyList())
        ViewCompat.setNestedScrollingEnabled(listCast, false)
    }

    private fun setupToolbar() {
        val toolbar = mBinding.toolbar
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            handleCollapsedToolbarTitle()
        }
    }


    /**
     * sets the title on the toolbar only if the toolbar is collapsed
     */
    private fun handleCollapsedToolbarTitle() {
        mBinding.appbar.addOnOffsetChangedListener(object : OnOffsetChangedListener {
            var isShow = true
            var scrollRange = -1
            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                // verify if the toolbar is completely collapsed and set the restaurant name as the title
                if (scrollRange + verticalOffset == 0) {
                    mBinding.collapsingToolbar.title =
                        mViewModel.restaurantLiveData.value!!.name
                    isShow = true
                } else if (isShow) { // display an empty string when toolbar is expanded
                    mBinding.collapsingToolbar.title = " "
                    isShow = false
                }
            }
        })
    }
}