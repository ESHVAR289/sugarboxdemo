package com.einfoplanet.demo.rest

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}