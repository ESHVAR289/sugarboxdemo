package com.einfoplanet.demo.rest

import androidx.annotation.NonNull
import androidx.annotation.Nullable


class ApiResponse(val status: Status, @Nullable val data: Any?, @Nullable error: Throwable?) {
    companion object {
        fun loading(): ApiResponse {
            return ApiResponse(
                Status.LOADING,
                null,
                null
            )
        }

        fun success(@NonNull data: Any): ApiResponse {
            return ApiResponse(
                Status.SUCCESS,
                data,
                null
            )
        }

        fun error(@NonNull error: Throwable): ApiResponse {
            return ApiResponse(
                Status.ERROR,
                null,
                error
            )
        }
    }
}