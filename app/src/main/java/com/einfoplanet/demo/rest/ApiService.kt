package com.einfoplanet.demo.rest

import android.util.Log
import com.einfoplanet.demo.BuildConfig
import com.einfoplanet.demo.model.Restaurant
import com.einfoplanet.demo.model.Restaurants
import io.reactivex.Observable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.io.IOException


interface ApiService {
    companion object {
        fun getApiService(): ApiService {
            val builder = OkHttpClient.Builder()
            if (BuildConfig.DEBUG) {
                val interceptor = HttpLoggingInterceptor { s -> Log.e("OkHttp", s) }
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
                builder.addInterceptor(interceptor)
            }
            builder.addInterceptor(AuthInterceptor())
            val client = builder
                .build()
            val BASE_URL = "https://developers.zomato.com/api/v2.1/"

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiService::class.java)
        }
    }

    @GET("geocode")
    fun getNearbyRestaurants(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double
    ): Observable<Restaurants>

    @GET("restaurant")
    fun getRestaurantDetail(
        @Query("res_id") restaurantId: String
    ): Observable<Restaurant>

    @GET("search")
    fun searchRestaurant(
        @Query("entity_type") entityType: String = "city",
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("category") category: String = "",
        @Query("sort") sort: String = "rating"
    ): Observable<Restaurants>
}

class AuthInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val url = request
            .url()
            .newBuilder()
            .build()

        request = request
            .newBuilder()
            .header("user-key", BuildConfig.ZOMATO_API_KEY)
            .header("Content-Type", "application/json")
            .header("Accept", "application/json")
            .url(url)
            .build()
        return chain.proceed(request)
    }
}