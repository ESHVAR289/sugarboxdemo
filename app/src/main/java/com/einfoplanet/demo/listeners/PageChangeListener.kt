package com.einfoplanet.demo.listeners

interface PageChangeListener {
    fun onPageChanged(selectedFragment: String)
}